from django.shortcuts import render,  get_object_or_404
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Conv
# This is where I am going to be making the magic happen

def init(request):
    
    for q in Conv.objects.all():
        q.delete()
    #return HttpResponseRedirect(reverse(''))

    
    c = Conv(weight_lbs = 14.5833)
    c.save()
    
    return HttpResponse(request)

def convert(request):
    
    for key in request.GET:
        print(f"{key} => {request.GET[key]}")
    
    if 'from' and 'value' and 'to' in request.GET:
    
        conv = toOz(float(request.GET['value']))
        response = { 'units' : request.GET['to'],
            'value' : conv,
        }
    else:
        response = { 'error' : 'you must supply a valid conversion request',

        }
    return JsonResponse(response)

def toOz(value):
    conversions = get_object_or_404(Conv)
    bruh = conversions.weight_lbs
    x = value * bruh
    print(x)
    return x

