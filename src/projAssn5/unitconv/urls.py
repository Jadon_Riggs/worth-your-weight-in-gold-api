from django.urls import path

from . import views

app_name = 'unitconv'


urlpatterns = [
               
    path('init', views.init, name="init"),
               
    #/unitconv/convert?to= from= value=
    path('convert', views.convert, name="convert"),
               
               
               
        ]

